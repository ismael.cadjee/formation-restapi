package com.datasensegroup.restfull.formation.service.dto;

/**
 * @author CADJEE ismael
 */
public class InfoCompte {

    public ClientDTO clientDTO;

    public CompteDTO compteDTO;

    public ClientDTO getClientDTO() {
        return clientDTO;
    }

    public void setClientDTO(ClientDTO clientDTO) {
        this.clientDTO = clientDTO;
    }

    public CompteDTO getCompteDTO() {
        return compteDTO;
    }

    public void setCompteDTO(CompteDTO compteDTO) {
        this.compteDTO = compteDTO;
    }
}
