package com.datasensegroup.restfull.formation.service.mapper;


import com.datasensegroup.restfull.formation.domain.*;
import com.datasensegroup.restfull.formation.service.dto.CompteDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link Compte} and its DTO {@link CompteDTO}.
 */
@Mapper(componentModel = "spring", uses = {ClientMapper.class})
public interface CompteMapper extends EntityMapper<CompteDTO, Compte> {

    @Mapping(source = "client.id", target = "clientId")
    CompteDTO toDto(Compte compte);

    @Mapping(source = "clientId", target = "client")
    Compte toEntity(CompteDTO compteDTO);

    default Compte fromId(Long id) {
        if (id == null) {
            return null;
        }
        Compte compte = new Compte();
        compte.setId(id);
        return compte;
    }
}
