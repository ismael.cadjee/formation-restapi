/**
 * View Models used by Spring MVC REST controllers.
 */
package com.datasensegroup.restfull.formation.web.rest.vm;
