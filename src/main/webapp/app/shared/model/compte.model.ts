export interface ICompte {
  id?: number;
  nom?: string;
  numero?: number;
  solde?: number;
  clientId?: number;
}

export class Compte implements ICompte {
  constructor(public id?: number, public nom?: string, public numero?: number, public solde?: number, public clientId?: number) {}
}
