import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [
    RouterModule.forChild([
      {
        path: 'compte',
        loadChildren: () => import('./compte/compte.module').then(m => m.ApiformationCompteModule),
      },
      {
        path: 'client',
        loadChildren: () => import('./client/client.module').then(m => m.ApiformationClientModule),
      },
      /* jhipster-needle-add-entity-route - JHipster will add entity modules routes here */
    ]),
  ],
})
export class ApiformationEntityModule {}
